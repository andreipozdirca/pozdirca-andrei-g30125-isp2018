package pozdirca-andrei-g30125.l8.e2;

import java.util.*;
import java.io.*;

public class Ex2 {

	public static void main(String[] args) throws IOException {
		System.out.print("Dati caracterul:");
		Scanner sc = new Scanner(System.in);
		char i = sc.next().charAt(0);
		BufferedReader in = new BufferedReader(new FileReader("data.txt"));
		String s, s2 = new String();
		while ((s = in.readLine()) != null)
			s2 += s + "\n";
		in.close();
		int count = 0;
		for (int j = 0; j < s2.length(); j++) {
			if (s2.charAt(j) == i) {
				count++;
			}
		}
		System.out.println("Total count=" + count);

	}

}