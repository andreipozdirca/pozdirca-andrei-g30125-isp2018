package pozdirca-andrei-g30125.l7.e2;

public class BankAccount {
	String owner;
	private double balance;

	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void withdraw(double amount) {
		setBalance(getBalance() - amount);
	}

	public void deposit(double amount) {
		setBalance(getBalance() + amount);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof BankAccount) {
			BankAccount b = (BankAccount) o;
			return owner == b.owner && balance == b.balance;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (int) balance + owner.hashCode();
	}

	public String toString() {
		return "Nume: " + owner + ", venit: " + balance;
	}

}