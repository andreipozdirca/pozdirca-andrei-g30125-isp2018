package g30125.pozdirca.andrei.l2.e1;

public class Ex6b {
	public static long factorial(int n) { 
	    if (n == 1) return 1; 
	    return n * factorial(n-1); 
	} 

}
