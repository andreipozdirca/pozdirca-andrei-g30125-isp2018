package pozdirca-andrei-g30125.l6.e3;

import java.awt.Color;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,50,"A3",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,100,50,"1",true);
        b1.addShape(s2);
        Shape s3= new Rectangle(Color.BLUE,150,100,200,"3",false);
        b1.addShape(s3);
        b1.delete("1");
        
      
    }
}