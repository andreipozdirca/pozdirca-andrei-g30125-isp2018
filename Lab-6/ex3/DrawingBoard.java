package pozdirca-andrei-g30125.l6.e3;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

	
    Shape[] shapes = new Shape[10];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }

        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }

    }
 
   public void delete (String id) {
	   for(int i=0;i<shapes.length;i++){
		   if(shapes[i].getId().equals(id)) 
			   shapes[i]=null;
		   
	   }
	   this.repaint();
   }
}