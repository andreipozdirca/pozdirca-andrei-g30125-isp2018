package pozdirca-andrei-g30125.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;


    public Rectangle(Color color, int length,int x, int y,String id,boolean filled) {
        super(color,x,y,id,filled);
        this.length = length;
    
    }
    
    public int getLength() {
        return length;
    }
    
   

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if(isFilled()==true)
        	g.fillRect(getX(), getY(), length, length);
        
    }
    
    
}