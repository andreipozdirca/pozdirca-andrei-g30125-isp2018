package pozdirca-andrei-g30125.l6.e1;

import java.awt.*;
public abstract class Shape {

    private Color color;
    private int x,y;
    String id;
    boolean filled;
    public Shape(Color color,int x,int y,String id,boolean filled) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.filled=filled;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public int getX() {
    	return x;
    }
    
    public int getY() {
    	return y;
    }
    public boolean isFilled() {
    	return filled;
    }

    public abstract void draw(Graphics g);
    
}