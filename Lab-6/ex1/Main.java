package pozdirca-andrei-g30125.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,50,"A3",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,100,50,"1",true);
        b1.addShape(s2);
        Shape s3= new Rectangle(Color.BLUE,150,100,200,"3",false);
        b1.addShape(s3);
        
        b1.delete("1");
        
      
    }
}
    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
    }
    
   public void delete (String id) {
	   for(int i=0;i<shapes.length;i++){
		   if(shapes[i].id.equals(id)) 
			   shapes[i]=null;
		   
	   }
	   this.repaint();
   }
}