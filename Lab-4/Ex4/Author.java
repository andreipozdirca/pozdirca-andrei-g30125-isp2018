package g30125.pozdirca.andrei.l4.e4;

public class Author {
          private  String name;
          private  String email;
          private  char gender;
          
          public Author(String name,String email,char gender) {
        	  this.name=name;
        	  this.email=email;
        	  this.gender=gender;
          }
          public String getName() {
        	  return this.name;
          }
          public String getEmail() {
        	  return this.email;
          }
          void setEmail() {
        	  this.email=email;
          }
          char getGender() {
        	  return this.gender;
          }
          public String toString() {
        	 return "Author "+this.name+" ("+this.gender+")"+" at "+this.email;
		
          }
		
}
