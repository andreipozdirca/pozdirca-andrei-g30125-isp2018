import becker.robots.City; 
import becker.robots.Direction; 
import becker.robots.Robot; 
import becker.robots.Thing; 
import becker.robots.Wall  
public class Ex5 {
 public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST); 
      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH); 
      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);  

      Wall blockAve4 = new Wall(ny, 1, 2, Direction.EAST); 
      Wall blockAve5 = new Wall(ny, 2, 1, Direction.SOUTH); 
      Wall blockAve6 = new Wall(ny, 1, 2, Direction.SOUTH);

      Robot mark = new Robot(ny, 1, 2, Direction.SOUTH);
      
 
 
      // mark goes around the roadblock
      mark.turnLeft();    
      mark.turnLeft();
      mark.turnLeft();    
      mark.move();

      mark.turnLeft();  

      mark.move();
      mark.turnLeft();   

      mark.move(); 

      mark.turnLeft();    
      mark.turnLeft();
      mark.turnLeft();    
      mark.move();
 