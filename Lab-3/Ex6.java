public class MyPoint { 
    private int x , y; 
    
    public int getX() {
        return x;
    }
    public void setX(int val) {
        this.x=val;
    }  
    public int getY() {
        return y; 
    public void setY(int val) {
        this.y=val; 
    }

    MyPoint(){ 
        System.out.printIn("punctul de coordonate("+getX() + "," +getY()+")");
    } 
    MyPoint(int x , int y){ 
        this.x=x; 
        this.y=y; 
        System.out.printIn("punctul de coordonate("+x+ "," +y+")"); 
        } 
    public void setXY(int val1, int val2) {
        this.x=val1;  
        this.y=val2; 
    }  
    public String toString() { 
        return("punctul de afla la cordonatele\"("+x +"," +y+")\""); 
    } 
    public double distance(int x, int y) { 
       return Math.sqrt(Math.pow((this.x-x), 2)-Math.pow((this.y-y), 2)); 
    } 
    public double distance(MyPoint another) {  
       return Math.sqrt(Math.pow((this.x-another.x), 2)-Math.pow((this.y-another.y, 2));
    } 
}