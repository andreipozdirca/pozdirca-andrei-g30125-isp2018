package pozdirca-andrei-g30125.l5.e3;

import java.util.ArrayList;
import java.util.Random;

public abstract class Sensor {
	private String location;
	
	public abstract int readValue();
	public String getLocation() {
		return this.location;	
	}
}
class TemperatureSensor extends Sensor{
	Random rt = new Random();
	int vt=rt.nextInt(100);
	public int readValue() {
		return this.vt;
	}
	
}
class LightSensor extends Sensor{
	Random rl = new Random();
	int vl=rl.nextInt(100);
	public int readValue() {
		return this.vl;
	}
	
}
class Controller{
	TemperatureSensor t=new TemperatureSensor();
	LightSensor l=new LightSensor();
	
	public void control() {
		System.out.println(t.readValue());
		System.out.println(l.readValue());
	}
	public static void main(String[] args) {
		int i=0;
		while(i<=20) {
		Controller c1 = new Controller();
		c1.control();
		i++;
		}
	}
}