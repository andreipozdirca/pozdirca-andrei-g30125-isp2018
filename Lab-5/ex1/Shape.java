package pozdirca-andrei-g30125.l5.e1;

public abstract class Shape {
    protected String color;
    protected boolean filled;
    
    
    public abstract double getArea();
    public abstract double getPerimeter();
    
    @Override
       public String toString() {
           return "Shape{" +
                   "color='" + color + '\'' +
                   ", filled=" + filled +
                   '}';
       }
    
    public Shape(){
        
    }
    public Shape(String color , boolean filled){
        this.color=color;
        this.filled=filled;
        
    }
    public String getColor(){
        return color;
        
    }
    public void setColor(String color){
        this.color=color;
    }
    public boolean isFilled(){
        return filled;
        
    }
    public void setFilled(boolean filled){
        this.filled=filled;
    }
    
}

class Circle extends Shape{
    protected double radius;
    
    public Circle(){
    
    }
    public Circle(double radius){
        this.radius=radius;
        
    }
    public Circle(double radius , String color , boolean filled){
        super(color,filled);
        this.radius=radius;
    
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius(double radius){
        this.radius=radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
        
    }
    public double getPerimeter(){
        return 2*Math.PI*radius;
        
    }
     @Override
       public String toString() {
           return "Circle{" +
                   "color='" + color + '\'' +
                   ", filled=" + filled +
                   ", radius=" + radius +
                   '}';
       }
}

class Rectangle extends Shape{
    protected double width;
    protected double length;

    public Rectangle(){
        
    }
    public Rectangle(double width , double length){
        this.width=width;
        this.length=length;
        
    }
    public Rectangle(double width , double length,String color , boolean filled){
        super(color,filled);
        this.width=width;
        this.length=length;
    }
    public double getWidth(){
        return width;
    }
    public void setWidth(double width){
        this.width=width;
    }
    public double getLength(){
        return length;
    }
    public void setLength(double length){
        this.length=length;
    }
    public double getArea(){
        return length*width;
    }
    public double getPerimeter(){
        return 2*(length+width);
    }
    @Override
       public String toString() {
           return "Rectangle{" +
                   "color='" + color + '\'' +
                   ", filled=" + filled +
                   ", width=" + width +
                   ", length=" + length +
                   '}';
       }
}

class Square extends Rectangle{
    protected  double side;
    public Square(){
        
    }
    public Square(double side){
        this.side=side;
    }
    public Square(double side , String color , boolean filled){
        this.color=color;
        this.side=side;
        this.filled=filled;
    }
    public double getSide(){
        return side;
    }
    public void setSide(double side){
        this.side=side;
    }
    public void SetWidth(double side){
        this.width=side;
    }
    public void setLength(double side){
        this.length=side;
    }
    @Override
       public String toString() {
           return "Square{" +
                   "color='" + color + '\'' +
                   ", side=" + side +
                   ", filled=" + filled +
                   '}';
       }
}