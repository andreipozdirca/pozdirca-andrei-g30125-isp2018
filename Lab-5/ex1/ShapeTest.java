package pozdirca-andrei-g30125.l5.e1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShapeTest {

	@Test
	public void testCircle() {
		Circle c=new Circle(1.0,"blue",true);
		assertEquals(c.toString(),"Circle{color='blue', filled=true, radius=1.0}");
	}
	
	@Test
	public void testRectangle() {
		Rectangle r=new Rectangle(1.0,1.0,"blue",true);
		assertEquals(r.toString(),"Rectangle{color='blue', filled=true, width=1.0, length=1.0}");
	}
	
	@Test
	public void testSquare() {
		Square s=new Square(5.0,"blue",false);
		assertEquals(s.toString(),"Square{color='blue', side=5.0, filled=false}");
	}

}